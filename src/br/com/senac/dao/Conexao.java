package br.com.senac.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author sala306b
 */
public class Conexao {

    private static Connection connection = null;
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DATABASE = "SysOficinaDosTapetes";
    private static final String URL = "jdbc:mysql://localhost:3306/" + DATABASE;
    private static final String LOGIN = "root";
    private static final String SENHA = "";

        public static Connection getConnection() throws SQLException, ClassNotFoundException {

        try {
            if (connection == null) {
                Class.forName(DRIVER);
                connection = DriverManager.getConnection(URL, LOGIN, SENHA);
            } else if (connection.isClosed()) {
                connection = DriverManager.getConnection(URL, LOGIN, SENHA);
            }
        } catch (ClassNotFoundException ex) {
            throw new ClassNotFoundException("Nao foi possivel carregar o Driver do banco");
        } catch (SQLException ex) {
            throw new SQLException("Falha ao conectar: " + ex.getMessage());
        }
        return connection;

    }

    public static void close() throws SQLException {
        try {
            connection.close();
        } catch (SQLException ex) {
            throw new SQLException("Falha ao desconectar:" + ex.getMessage());
        }
    }

}
