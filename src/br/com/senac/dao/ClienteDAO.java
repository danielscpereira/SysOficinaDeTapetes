/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.model.Cliente;
import br.com.senac.model.PessoaFisica;
import br.com.senac.model.PessoaJuridica;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sala306b
 */
public class ClienteDAO extends DAO<Cliente> {

    @Override
    public void save(Cliente cliente) throws Exception {

        if (cliente instanceof PessoaFisica) {
            this.savePessoaFisica(cliente);
        } else {
            this.savePessoaJuridica(cliente);
        }
        
    }

    @Override
    public void update(Cliente cliente) throws Exception {
         if(cliente instanceof PessoaFisica){
                this.updatePessoaFisica(cliente);
            }else {
                this.updatePessoaJuridica(cliente);
            }
    }

    @Override
    public void delete(Cliente cliente) throws Exception {
        if(cliente instanceof PessoaFisica){
                this.updatePessoaFisica(cliente);
            }else {
                this.updatePessoaJuridica(cliente);
            }
    }

    @Override
    public Cliente getById(long id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cliente> listAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //Save PessoaFisica e PessoaJuridica//
    private void savePessoaFisica(Cliente cliente) {

        Connection connection;
        try {
            connection = Conexao.getConnection();

            PessoaFisica pessoaFisica = (PessoaFisica) cliente;

            String query = "insert into cliente (Nome , Cpf , Rg) values ( ? , ? , ? )";

            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, pessoaFisica.getNome());
            ps.setString(2, pessoaFisica.getCpf());
            ps.setString(3, pessoaFisica.getRg());

        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void savePessoaJuridica(Cliente cliente) {

        Connection connection;
        try {
            connection = Conexao.getConnection();

            PessoaJuridica pessoaJuridica = (PessoaJuridica) cliente;

            String query = "insert into cliente (Nome , Cnpj , InscricaoEstadual, NomeFantasia) values ( ? , ? , ? , ? )";

            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, pessoaJuridica.getNome());
            ps.setString(2, pessoaJuridica.getCnpj());
            ps.setString(3, pessoaJuridica.getInscricaoEstadual());
            ps.setString(3, pessoaJuridica.getNomeFantasia());

        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    //Atualizar PessoaFisica e PessoaJuridica//
      private void updatePessoaFisica(Cliente cliente) {

        Connection connection;
        try {
            connection = Conexao.getConnection();

            PessoaFisica pessoaFisica = (PessoaFisica) cliente;

            String query = "update into cliente (Nome , Cpf , Rg) values ( ? , ? , ? )";

            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, pessoaFisica.getNome());
            ps.setString(2, pessoaFisica.getCpf());
            ps.setString(3, pessoaFisica.getRg());

        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
        private void updatePessoaJuridica(Cliente cliente) {

        Connection connection;
        try {
            connection = Conexao.getConnection();

            PessoaFisica pessoaFisica = (PessoaFisica) cliente;

            String query = "update into cliente (Nome , Cpf , Rg) values ( ? , ? , ? )";

            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, pessoaFisica.getNome());
            ps.setString(2, pessoaFisica.getCpf());
            ps.setString(3, pessoaFisica.getRg());

        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    //Deletar PessoaFisica e PessoaJuridica//
        
         private void deletePessoaFisica(Cliente cliente) {

        Connection connection;
        try {
            connection = Conexao.getConnection();

            PessoaFisica pessoaFisica = (PessoaFisica) cliente;

            String query = "delete into cliente (Nome , Cpf , Rg) values ( ? , ? , ? )";

            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, pessoaFisica.getNome());
            ps.setString(2, pessoaFisica.getCpf());
            ps.setString(3, pessoaFisica.getRg());

        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
        
          private void deletePessoaJuridica(Cliente cliente) {

        Connection connection;
        try {
            connection = Conexao.getConnection();

            PessoaFisica pessoaFisica = (PessoaFisica) cliente;

            String query = "delete into cliente (Nome , Cpf , Rg) values ( ? , ? , ? )";

            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, pessoaFisica.getNome());
            ps.setString(2, pessoaFisica.getCpf());
            ps.setString(3, pessoaFisica.getRg());

        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
        
    
         

}
