/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.model.Servico;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala306b
 */
public class ServicoDAO extends DAO<Servico> {

    @Override
    public void save(Servico servico) throws SQLException, ClassNotFoundException {

        Connection connection = Conexao.getConnection();

        String query = "INSERT INTO SERVICO (Descricao , PrecoPadrao) VALUES ( ? , ? ); ";
        PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

        ps.setString(1, servico.getDescricao());
        ps.setDouble(2, servico.getPrecoPadrao());

        ps.execute();

    }

    @Override
    public void update(Servico servico) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "UPDATE SERVICO " + "SET DESCRICAO = ? ," + "PRECOPADRAO = ? " + " WHERE ID = ?";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setString(1, servico.getDescricao());
        ps.setDouble(2, servico.getPrecoPadrao());
        ps.executeUpdate();

    }

    @Override
    public void delete(Servico servico) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "DELETE FROM SERVICO WHERE ID = ?";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setLong(1, servico.getId());

        ps.executeUpdate();
    }

    @Override
    public Servico getById(long id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Servico> listAll() throws Exception {
        List<Servico> listaTipoServico = new ArrayList<>();

        Connection connection = Conexao.getConnection();

        String query = "SELECT * FROM  categoria ";

        Statement statement = connection.createStatement();

        ResultSet rs = statement.executeQuery(query);
        
        while(rs.next()){
            long id = rs.getLong("id");
            String descricao = rs.getString("Descricao");
            Servico servico = new Servico(descricao, id, null, id, true);
            listaTipoServico.add(servico);
            
        }
        
        return listaTipoServico;
        
        

    }

}
