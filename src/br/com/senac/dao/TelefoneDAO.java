/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.model.Telefone;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sala306b
 */
public class TelefoneDAO extends DAO<Telefone> {

    @Override
    public void save(Telefone telefone) throws SQLException, ClassNotFoundException {

        Connection connection = Conexao.getConnection();

        String query = "INSERT INTO TELEFONE (Telefone) VALUES (?); ";
        PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

        ps.setString(1, telefone.getNumero());

        ps.execute();
    }

    @Override
    public void update(Telefone telefone) throws Exception {

        Connection connection = Conexao.getConnection();

        String query = "UPDATE TELEFONE " + "SET TELEFONE = ? ," + "WHERE ID = ?";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setString(1, telefone.getNumero());

        ps.executeUpdate();
    }

    @Override
    public void delete(Telefone telefone) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "DELETE FROM TELEFONE WHERE ID = ?";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setString(1, telefone.getNumero());

        ps.executeUpdate();
    }

    @Override
    public Telefone getById(long id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Telefone> listAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
