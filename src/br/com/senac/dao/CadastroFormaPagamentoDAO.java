/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.dao.Conexao;
import br.com.senac.dao.DAO;
import br.com.senac.model.Categoria;
import br.com.senac.model.FormaPagamento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala306b
 */
public class CadastroFormaPagamentoDAO extends DAO<FormaPagamento>{
   
    @Override
    public void update(FormaPagamento fp) throws Exception {
       Connection connection = Conexao.getConnection();
       
       String query = "UPDATE formapagamento SET Descricao = ?, isParcela = ?,QuantidadeMaxima =?, where id = ?";
       
        PreparedStatement ps = connection.prepareStatement(query);

        ps.setString(1, fp.getDescricao());
        ps.setBoolean(2, fp.isParcela());
        ps.setInt(3,fp.getQuantidadeMaximaParcela());
        ps.setLong(4,fp.getId());
        ps.executeUpdate();
        
        
        
        
    }
    
    @Override
    public void save(FormaPagamento fp) throws Exception {
         Connection connection = Conexao.getConnection();
        
        String query = "INSERT INTO formapagamento(Descricao ,isParcela ,QuantidadeMaxima, Ativo) values (?,?,?,?)";
        
         PreparedStatement ps = connection.prepareStatement(query);
         
         ps.setString(1,fp.getDescricao());
         ps.setBoolean(2,fp.isParcela());
         ps.setInt(3, fp.getQuantidadeMaximaParcela());
         ps.setBoolean(4, fp.isAtivo());
         
          ps.executeUpdate();
          
          ResultSet rs = ps.getGeneratedKeys();
          ps.execute(query, Statement.RETURN_GENERATED_KEYS);
          rs.first();
          
          long id = rs.getLong(1);
          fp.setId(id);
          connection.close();
          
       
    }  

    @Override
    public void delete(FormaPagamento objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FormaPagamento getById(long id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<FormaPagamento> listAll() throws Exception {
       List<FormaPagamento> listaFormaPagamento = new ArrayList<>();
       Connection connection = Conexao.getConnection();
       String query = "SELECT * From formapagamento";
       Statement statement = connection.createStatement();

        ResultSet rs = statement.executeQuery(query);
        
        while (rs.next()) {
            String Descricao= rs.getString("descricao");
            boolean isParcela = rs.getBoolean("parcela");
            int quantidadeMaxima = rs.getInt("QuantidadeMaxima");
            long id = rs.getLong("id");
            boolean ativo = rs.getBoolean("Ativo");
            
            FormaPagamento formapagamento = new FormaPagamento(Descricao, isParcela, quantidadeMaxima,id, ativo);
            
                  }
        
        return null;
    }
    
    
      public List<FormaPagamento> listaByName(String nome) throws Exception {
        List<FormaPagamento> listaFormaPagamentos = new ArrayList<>();
        try {
            Connection connection = Conexao.getConnection();
            String query = "SELECT * FROM formapagamento where descricao = :";

            PreparedStatement ps = connection.prepareCall(query);

            ps.setString(1, nome);
            
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
               
                FormaPagamento formaPagamento = new FormaPagamento();
                formaPagamento.setId(rs.getLong("id"));
                formaPagamento.setDescricao(rs.getString("Descricao"));
                formaPagamento.setParcela(rs.getBoolean("isParcela"));
                formaPagamento.setQuantidadeMaximaParcela(rs.getInt("QuantidadeMaxima"));
                formaPagamento.setAtivo(rs.getBoolean("Ativo"));

               listaFormaPagamentos.add(formaPagamento);

            }
            connection.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaFormaPagamentos;
    }

   

   
    
}
