/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.model.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author sala306b
 */
public class EntregaDao extends DAO<Entrega> {

    @Override
    public void save( Entrega entrega ) throws Exception {

        Connection connection = Conexao.getConnection();
        connection.setAutoCommit( false );
        try {

            PreparedStatement ps = null;

            if ( entrega.getId() == 0 ) {

                String query = "INSERT INTO sysoficinadostapetes.endereco "
                               + "(Logradouro,Cep,Bairro,Cidade,Numero,Complemento,idUf,idTipoEndereco,Ativo) "
                               + "VALUES "
                               + "(?,?,?,?,?,?,?,?,?);";

                ps = connection.prepareStatement( query, PreparedStatement.RETURN_GENERATED_KEYS );
                ps.setString( 1, entrega.getEndereco().getLogradouro() );
                ps.setLong( 2, entrega.getEndereco().getCep() );
                ps.setString( 3, entrega.getEndereco().getBairro() );
                ps.setString( 4, entrega.getEndereco().getCidade() );
                ps.setInt( 5, entrega.getEndereco().getNumero() );
                ps.setString( 6, entrega.getEndereco().getComplemento() );
                ps.setLong( 7, entrega.getEndereco().getUf().getId() );
                ps.setLong( 8, TipoEndereco.Entrega );
                ps.setBoolean( 9, true );

                ps.executeUpdate();

                ResultSet rs = ps.getGeneratedKeys();
                rs.first();

                long idEndereco = rs.getLong( 1 );

        
                entrega.getEndereco().setId( idEndereco );

            }

            String queryEntrega = "insert into entrega(DataEntrega,"
                                  + "Observacoes , idEndereco";

            ps = connection.prepareStatement( queryEntrega );

            ps.setDate( 1, new java.sql.Date( entrega.getDataEntrega().getTime() ) );
            ps.setString( 2, entrega.getObservacaoes() );
            ps.setLong( 3, entrega.getEndereco().getId() );

            connection.commit();

        } catch ( SQLException ex ) {
            connection.rollback();
            throw ex;
        }

    }

    @Override
    public void update( Entrega entrega ) throws Exception {
        throw new UnsupportedOperationException( "Not supported yet." ); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete( Entrega entrega ) throws Exception {
        throw new UnsupportedOperationException( "Not supported yet." ); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Entrega getById( long id ) throws Exception {
        
        return null;
    }

    @Override
    public List<Entrega> listAll() throws Exception {
        
        
        return null;
        
    }

}
