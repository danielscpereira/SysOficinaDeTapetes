/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import java.util.List;

/**
 *
 * @author sala306b
 */
public abstract class DAO<T> {
    
    public abstract void save(T objeto) throws Exception; 
    public abstract void update(T objeto) throws Exception; 
    public abstract void delete(T objeto) throws Exception; 
    public abstract T getById(long id)throws Exception;
    public abstract List<T> listAll() throws Exception; 
    
}


















