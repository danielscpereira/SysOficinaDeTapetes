/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.model.Uf;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala306b
 */
public class UfDao extends DAO<Uf>{

    @Override
    public void save(Uf objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Uf objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Uf objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Uf getById(long id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Uf> listAll() throws Exception {
        
        List<Uf> listaUf = new ArrayList<>();
        
        Connection connection = Conexao.getConnection() ; 
        
        String query = "Select * from uf" ; 
        
        Statement statement = connection.createStatement() ; 
        
        ResultSet rs = statement.executeQuery(query) ; 
        
        while (rs.next()) {
            
            long id = rs.getLong("id") ; 
            String estado = rs.getString("estado");
            String sigla  = rs.getString("sigla");
            boolean ativo = rs.getBoolean("ativo") ; 
            
            Uf uf = new Uf(id, ativo, estado, sigla);
            
            listaUf.add(uf);
            
            
        }
        
        return listaUf ; 
        
    }
    
}
