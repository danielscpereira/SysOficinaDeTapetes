/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.model.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author sala306b
 */
public class PedidosDAO extends DAO<Pedido> {
    
    @Override
    public void save(Pedido pedido) throws Exception {
        
        Connection connection = Conexao.getConnection();
        
        
        connection.setAutoCommit(false);
        
        try{
        
        String query = "INSERT INTO pedido (idCliente, DataEntrada,DataPrevistaEntrega, "
                + "DataFechamento, Observacoes, idFormaPagamento, QuantidadeParcela, IsRetira, "
                + " idUsuarioInclusao, DataUltimaAtualizacao, Ativo) "
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        
        PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
        
        ps.setLong(1, pedido.getId());
        ps.setDate(2, new java.sql.Date(pedido.getDataEntrada().getTime()));
        ps.setDate(3, new java.sql.Date(pedido.getDataPrevista().getTime()));
        ps.setDate(4, new java.sql.Date(pedido.getDataFechamento().getTime()));
        ps.setString(5, pedido.getObservação());
        ps.setLong(6, pedido.getFormaPagamento().getId()); //aguardando atualização
        ps.setInt(7, pedido.getQuantidadeParcelas()); //aguardando atualização
        ps.setBoolean(8, pedido.isRetira());
        ps.setLong(9, pedido.getUsuarioInclusao().getId());
        ps.setBoolean(10, pedido.isAtivo());
        
        ps.executeUpdate();
        
        ResultSet rs = ps.getGeneratedKeys();
        
        rs.first();
        
        long idPedido = rs.getLong(1);
        
        pedido.setId(idPedido);

        /*Listagem de intens: */
        String queryItem = "Insert into itemPedido "
                + "(idServico , quantidade , precounitario) "
                + "values "
                + "(? , ? , ? )";
        
        ps = connection.prepareStatement(queryItem);
        
        for (ItemDePedido item : pedido.getItens()) {
            
            ps.setLong(1, idPedido);
            ps.setLong(2, item.getId());
            ps.setInt(3, item.getQuantidade());
            ps.setDouble(4, item.getPrecoUnitario());
            
            ps.executeUpdate() ; 
            
        }
        
        connection.commit();
        
        }catch(SQLException ex){
            connection.rollback();
            throw ex ; 
        }
        
        
    }
    
    
//    public static void main(String args[]){
//        
//        Pedido pedido = new Pedido(new Cliente("das", "dasd", null, 1, true) {
//        }, null, null, null, null, quantidadeParcelas, true, null, null, null, null, null, null, id, true)
//        
//        
//    }
    
    
    
    @Override
    public void update(Pedido objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void delete(Pedido objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Pedido getById(long id) throws Exception {
        
        Pedido pedido = null;
        
        Connection connection = Conexao.getConnection();
        
        String query = "Select p.id  , p.DataEntrada , p.DataFechamento , p.Observacoes  , p.quantidadeParcela , p.isretira , p.DataUltimaAtualizacao, p.Ativo  , "
                + "f.id idFormaPagamento  , f.Descricao  , "
                + "u1.id idUsuarioInclusao , u1.NomeUsuarioInclusao , "
                + "u2.id idUsuarioFechamento , u2.NomeUsuarioFechamento "
                + "from pedido p  "
                + "inner join cliente c "
                + "on p.idCliente = c.id "
                + "inner join formapagamento f "
                + "on p.idFormaPagamento  = f.id "
                + "inner join usuario u1 "
                + "on p.idUsuarioInclusao = u1.id "
                + "inner join usuario u2  "
                + "on p.idUsuarioFechamento = u2.id "
                + "where p.id = ? ;";
        
        PreparedStatement ps = connection.prepareStatement(query);
        
        ps.setLong(1, id);
        
        ResultSet rs = ps.executeQuery();
        
        if (rs.first()) {

//            pedido = new Pedido(
//                    rs.getLong( "id"), 
//                    rs.getBoolean( "ativo"), 
//                    rs.getDate( "dataEntrada"), 
//                    rs.getDate( "dataFechamento"), 
//                    rs.getDate( "dataPrevista"), 
//                    rs.getString( "observacao"),
//                    rs.getInt( "quantidadeParcela"),
//                    rs.getBoolean( "IsRetira"),
//                    rs.getDate( "dataUltimaAtualizacao"),
//                    new Usuario(rs.getLong( "idUsuarioInclusao"), rs.getString( "NomeUsuarioInclusao")),
//                    new Usuario(rs.getLong( "idUsuarioFechamento"), rs.getString( "NomeUsuarioFechamento")));
            String queryItens = "select s.id , s.Descricao , i.quantidade , i.PrecoUnitario  "
                    + "from itemdepedido i "
                    + "inner join servico s "
                    + "on  i.idServico = s.id";
            
            ps = connection.prepareStatement(queryItens);
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                
                long idServico = rs.getLong("id");
                String descricao = rs.getString("descricao");
                int quantidade = rs.getInt("quantidade");
                double precoUnitario = rs.getDouble("PrecoUnitario");

                //   ItemDePedido item = new ItemDePedido(quantidade, precoUnitario )
            }
            
        }
        
        return pedido;
        
    }
    
    @Override
    public List<Pedido> listAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
