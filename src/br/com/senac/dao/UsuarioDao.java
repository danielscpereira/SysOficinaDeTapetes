/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.dao.Conexao;
import br.com.senac.dao.DAO;
import br.com.senac.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala306b
 */
public class UsuarioDao extends DAO<Usuario> {

    @Override
    public void save(Usuario usuario) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "insert into usuario (Nome , Cpf , Login, Senha) "
                + "			values (? , ? , ?, ?);";

        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, usuario.getNome());
        ps.setString(2, usuario.getCpf());
        ps.setString(3, usuario.getLogin());
        ps.setString(4, usuario.getSenha());

        ps.executeUpdate();

        ps.execute(query, Statement.RETURN_GENERATED_KEYS);

        ResultSet rs = ps.getGeneratedKeys();
        rs.first();

        long id = rs.getLong(1);

        usuario.setId(id);
    }

    @Override
    public void update(Usuario usuario) throws Exception {

        Connection connection = Conexao.getConnection();

        String query = "UPDATE usuario SET Login = ?, Senha = ? WHERE id = 3;";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setString(1, usuario.getLogin());
        ps.setString(2, usuario.getSenha());

        ps.executeUpdate();

    }

    @Override
    public void delete(Usuario usuario) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "delete from usuario where id = ?;";

        PreparedStatement ps = connection.prepareStatement(query);
        ps.setLong(1, usuario.getId());

        ps.executeUpdate();

    }

    @Override
    public Usuario getById(long id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Usuario> listAll() throws Exception {
        List<Usuario> listaUsuario = new ArrayList<>();

        Connection connection = Conexao.getConnection();

        String query = "Select id ,  nome , Cpf , Login  From contatos ; ";

        Statement statement = connection.createStatement();

        ResultSet rs = statement.executeQuery(query);

        while (rs.next()) {

            long id = rs.getLong("id");
            String nome = rs.getString("nome");
            String cpf = rs.getString("Cpf");
            String login = rs.getString("login");
            Usuario usuario = new Usuario(login, id, nome, cpf);
            listaUsuario.add(usuario);
        }
        return listaUsuario;
    }

    public List<Usuario> getByName(String nomeBuscado) throws Throwable {

        List<Usuario> listaUsuario = new ArrayList<>();

        Connection connection = Conexao.getConnection();

        String query = "Select id , "
                + "Nome ,"
                + " Cpf ,"
                + " Login  "
                + "From usuarios "
                + "where 1 = 1 ";

        if (nomeBuscado != null) {
            query += " and nome like '" + nomeBuscado + "%' ;";
        }
        Statement statement = connection.createStatement();

        ResultSet rs = statement.executeQuery(query);

        while (rs.next()) {

            long id = rs.getLong("id");
            String nome = rs.getString("nome");
            String cpf = rs.getString("Cpf");
            String login = rs.getString("login");
            Usuario usuario = new Usuario(login, id, nome, cpf);
            listaUsuario.add(usuario);
        }
        return listaUsuario;
    }

    public List<Usuario> getByCpf(String cpfBuscado) throws Throwable {

        List<Usuario> listaUsuario = new ArrayList<>();

        Connection connection = Conexao.getConnection();

        String query = "Select nome  "
                + "From usuario "
                + "where 1=1 ";

        if (cpfBuscado != null) {
            query += " and cpf = ?";
        }

        PreparedStatement ps = connection.prepareStatement(query);

        if (cpfBuscado != null) {
            ps.setString(1, cpfBuscado);
        }

        ResultSet rs = ps.executeQuery();
        while (rs.next()) {

            long id = rs.getLong("id");
            String nome = rs.getString("nome");
            String cpf = rs.getString("Cpf");
            String login = rs.getString("login");
            Usuario usuario = new Usuario(login, id, nome, cpf);
            listaUsuario.add(usuario);
        }

        return listaUsuario;
    }

}
