/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.model.TipoTelefone;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala306b
 */
public class TipoTelefoneDAO extends DAO<TipoTelefone> {

    /**
     *
     * @param tipoTelefone
     * @throws Exception
     */
    @Override
    public void save(TipoTelefone tipoTelefone) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "INSERT INTO TIPOTELEFONE (descricao , ativo) VALUES (? , ?)";

        PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

        ps.setString(1, tipoTelefone.getDescricao());
        ps.setBoolean(2, tipoTelefone.isAtivo());

        ps.executeUpdate();

        //ResultSet rs = ps.getGeneratedKeys();

        //rs.first();

        //tipoTelefone.setId(rs.getInt(1));

    }

    @Override
    public void update(TipoTelefone tipoTelefone) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "UPDATE TIPOTELEFONE "
                + "SET DESCRICAO = ? ,"
                + "Ativo = ?"
                + "WHERE ID = ?";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setString(1, tipoTelefone.getDescricao());
        ps.setBoolean(2, tipoTelefone.isAtivo());
        ps.setLong(3, tipoTelefone.getId());

        ps.executeUpdate();

    }

    @Override
    public void delete(TipoTelefone tipoTelefone) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "DELETE FROM TIPOTELEFONE WHERE ID = ?";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setLong(1, tipoTelefone.getId());

        ps.executeUpdate();
    }

    @Override
    public TipoTelefone getById(long id) throws Exception {
        TipoTelefone tipoTelefone = null;

        Connection connection = Conexao.getConnection();

        String query = "SELECT * FROM TIPOTELEFONE WHERE ID = ?";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setLong(1, id);

        ResultSet rs = ps.executeQuery();

        if (rs.first()) {
            String descricao = rs.getString("descicao");
            boolean ativo = rs.getBoolean("ativo");
            tipoTelefone = new TipoTelefone(descricao, id, ativo);

        }

        return tipoTelefone;
    }

    @Override
    public List<TipoTelefone> listAll() throws Exception {
        List<TipoTelefone> listaTipoTelefone = new ArrayList<>();

        Connection connection = Conexao.getConnection();

        String query = "SELECT * FROM TIPOTELEFONE ";

        Statement statement = connection.createStatement();

        ResultSet rs = statement.executeQuery(query);

        while (rs.next()) {

            long id = rs.getLong("id");
            String descricao = rs.getString("descricao");
            boolean ativo = rs.getBoolean("ativo");
            TipoTelefone tipoTelefone = new TipoTelefone(descricao, id, ativo);
            listaTipoTelefone.add(tipoTelefone);

        }

        return listaTipoTelefone;

    }

    public TipoTelefone getByDescricao(String descricao) throws Throwable {

        TipoTelefone tipoTelefone = null;

        Connection connection = Conexao.getConnection();

        String query = "SELECT * FROM TIPOTELEFONE WHERE DESCRICAO =  ? ";

        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, descricao);

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            long id = rs.getLong("id");
            boolean ativo = rs.getBoolean("ativo");
            tipoTelefone = new TipoTelefone(descricao, id, ativo);
        }

        return tipoTelefone;

    }

}
