/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.model.Categoria;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoriaDAO extends DAO<Categoria> {

    @Override
    public void save(Categoria categoria) throws Exception {
        Connection connection = Conexao.getConnection();
        String query = "INSERT INTO CATEGORIA (Ativo ,Descricao ) values (?, ?)";
        PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
        ps.setBoolean(1, categoria.isAtivo());
        ps.setString(2, categoria.getDescricao());

        ps.executeUpdate();

        ResultSet rs = ps.getGeneratedKeys();
        rs.first();

        long id = rs.getLong(1);

        categoria.setId(id);

        connection.close();
    }

    @Override
    public void update(Categoria categoria) throws Exception {
//        try {
//            Connection connection = Conexao.getConnection();
//        
//        String query = "UPDATE CATEGORIA SET DESCRICAO = ?, ATIVO = ? WHERE ID = ?;";
//        PreparedStatement statement = connection.prepareStatement(query);
//        
//       
//        statement.setString(1,categoria.getDescricao());
//        statement.setBoolean(2,categoria.isAtivo());
//         statement.setLong(3,categoria.getId());
//       
//        statement.execute();
//        statement.close();
//        } catch (Exception ex) {
//           ex.getStackTrace();
//        }
//        
        Connection connection = Conexao.getConnection();

        String query = "update categoria "
                + "set descricao = ? , "
                + "ativo= ?  "
                + "where id = ? ;";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setString(1, categoria.getDescricao());
        ps.setBoolean(2, categoria.isAtivo());
        ps.setLong(3, categoria.getId());

        ps.executeUpdate();

    }

    public void delete(Categoria categoria) throws Exception {

        try {

            Connection connection = Conexao.getConnection();
            String query = "DELETE FROM CATEGORIA WHERE ID = ?";

            PreparedStatement statement = connection.prepareStatement(query);

            statement.setLong(1, categoria.getId());
            statement.execute();
            statement.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public Categoria getById(long id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Categoria> listaByName(String nome) throws Exception {
        List<Categoria> listaCategorias = new ArrayList<>();
        try {
            Connection connection = Conexao.getConnection();
            String query = "SELECT * FROM CATEGORIA where descricao = :";

            PreparedStatement ps = connection.prepareCall(query);

            ps.setString(1, nome);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Categoria categoria = new Categoria();
                categoria.setId(rs.getLong("id"));
                categoria.setDescricao(rs.getString("Descricao"));
                categoria.setAtivo(rs.getBoolean("Ativo"));

                listaCategorias.add(categoria);

            }
            connection.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaCategorias;
    }

    @Override
    public List<Categoria> listAll() throws Exception {
        List<Categoria> listaCategorias = new ArrayList<>();
        Connection connection = Conexao.getConnection();
        String query = "SELECT * FROM CATEGORIA ; ";

        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(query);

        while (rs.next()) {
            Categoria categoria = new Categoria();
            categoria.setId(rs.getLong("id"));
            categoria.setDescricao(rs.getString("Descricao"));
            categoria.setAtivo(rs.getBoolean("Ativo"));
            listaCategorias.add(categoria);
        }

        return listaCategorias;

    }

}
