/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.dao;

import br.com.senac.model.TipoEndereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala306b
 */
public class TipoEnderecoDao extends DAO<TipoEndereco> {

    @Override
    public void save(TipoEndereco tipoEndereco) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "insert into tipoendereco (Descricao, Ativo) values (?, ?);";

        PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

        ps.setString(1, tipoEndereco.getDescricao());
        ps.setBoolean(2, tipoEndereco.isAtivo());

        ps.execute();

        //ResultSet rs = ps.getGeneratedKeys();
        // rs.first();
        // tipoEndereco.setId(rs.getInt(1));
    }

    @Override
    public void update(TipoEndereco tipoEndereco) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "UPDATE TIPOENDERECO "
                + "SET DESCRICAO = ? ,"
                + "ATIVO = ? "
                + " WHERE ID = ?";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setString(1, tipoEndereco.getDescricao());
        ps.setBoolean(2, tipoEndereco.isAtivo());
        ps.setLong(3, tipoEndereco.getId());

        ps.executeUpdate();
    }

    @Override
    public void delete(TipoEndereco tipoEndereco) throws Exception {
        Connection connection = Conexao.getConnection();

        String query = "DELETE FROM TIPOENDERECO WHERE ID = ?";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setLong(1, tipoEndereco.getId());

        ps.executeUpdate();
    }

    @Override
    public TipoEndereco getById(long id) throws Exception {
        TipoEndereco tipoEndereco = null;

        Connection connection = Conexao.getConnection();

        String query = "SELECT * FROM TIPOENDERECO WHERE ID = ?";

        PreparedStatement ps = connection.prepareStatement(query);

        ps.setLong(1, id);

        ResultSet rs = ps.executeQuery();

        if (rs.first()) {
            String descricao = rs.getString("descricao");
            boolean ativo = rs.getBoolean("ativo");
            tipoEndereco = new TipoEndereco(descricao, id, ativo);

        }

        return tipoEndereco;
    }

    @Override
    public List<TipoEndereco> listAll() throws Exception {
        List<TipoEndereco> listaTipoEndereco = new ArrayList<>();

        Connection connection = Conexao.getConnection();

        String query = "SELECT * FROM TIPOENDERECO ";

        Statement statement = connection.createStatement();

        ResultSet rs = statement.executeQuery(query);

        while (rs.next()) {

            long id = rs.getLong("id");
            String descricao = rs.getString("descricao");
            boolean ativo = rs.getBoolean("ativo");
            TipoEndereco tipoEndereco = new TipoEndereco(descricao, id, ativo);
            listaTipoEndereco.add(tipoEndereco);
        }

        return listaTipoEndereco;

    }

    public TipoEndereco getByDescricao(String descricao) throws Throwable {

        TipoEndereco tipoEndereco = null;

        Connection connection = Conexao.getConnection();

        String query = "SELECT * FROM TIPOENDERECO WHERE DESCRICAO =  ? ";

        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, descricao);

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            long id = rs.getLong("id");
            boolean ativo = rs.getBoolean("ativo");

            tipoEndereco = new TipoEndereco(descricao, id, ativo);
        }

        return tipoEndereco;

    }

    public List<TipoEndereco> getByFiltro(String descricao, String situacao) throws Throwable {

        List<TipoEndereco> listaTipoEndereco = new ArrayList<>();

        Connection connection = Conexao.getConnection();

        String query = "SELECT * FROM TIPOENDERECO WHERE 1 = 1 ";

        if (descricao != null) {
            query += " And descricao like ? ";
        }

        if (situacao != null) {
            query += " And Ativo  = ?";
        }

        PreparedStatement ps = connection.prepareStatement(query);

        int parameter = 0;

        if (descricao != null) {
            ps.setString(++parameter, descricao + "%");
        }

        if (situacao != null) {
            boolean ativo = situacao.equals("Ativo");
            ps.setBoolean(++parameter, ativo);
        }

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            TipoEndereco tipoEndereco = new TipoEndereco(rs.getString("descricao"), rs.getLong("id"), rs.getBoolean("ativo"));
            listaTipoEndereco.add(tipoEndereco);
        }

        return listaTipoEndereco;

    }

}
