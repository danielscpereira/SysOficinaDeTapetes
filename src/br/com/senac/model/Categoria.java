/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

import java.util.List;

/**
 *
 * @author daniel
 */
public class Categoria extends Entidade {

    private String descricao;

    public Categoria() {
        this.ativo = true;

    }

    public Categoria(String descricao, long id, boolean ativo) {
        super(id, ativo);
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String toString() {

        return this.descricao;

    }
}
