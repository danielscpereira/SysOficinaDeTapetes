/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala306b
 */
public abstract class Cliente extends Entidade {

    protected String nome;
    protected String email;
    protected List<Endereco> enderecos;
    protected List<Telefone> telefones;

    public Cliente() {
        this.enderecos = new ArrayList<>();
        this.telefones = new ArrayList<>();
    }

    public Cliente(long id, boolean ativo) {
        super(id, ativo);
    }

    public Cliente(String nome, String email, List<Endereco> enderecos, long id, boolean ativo) {
        super(id, ativo);
        this.nome = nome;
        this.email = email;
        this.enderecos = enderecos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    public void adicionarTelefone(Telefone telefone) {
        this.telefones.add(telefone);
    }

    public void removerTelefone(Telefone telefone) {
        this.telefones.remove(telefone);
    }

    public void adicionarEndereco(Endereco endereco) {
        this.enderecos.add(endereco);
    }

    public void removerEndereco(Endereco endereco) {
        this.enderecos.remove(endereco);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Endereco getEnderecoPrincipal() {

        for (Endereco endereco : this.getEnderecos()) {
            if (endereco.isPadrao()) {
                return endereco;
            }
        }

        return null;

    }

}
