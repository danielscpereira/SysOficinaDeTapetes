/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 *
 *
 */
public class Pedido extends Entidade {

    private Cliente cliente;
    private Date dataEntrada;
    private Date dataPrevista;
    private Date dataFechamento;
    private String observação;
    private int quantidadeParcelas;
    private boolean retira;
    private FormaPagamento formaPagamento;
    private List<ItemDePedido> itens;
    private Entrega entrega;
    private Date dataUltimaAtualizacao;
    private Usuario usuarioInclusao;
    private Usuario usuarioFechamento;

    public Pedido() {
        this.dataEntrada = new Date();
        this.itens = new ArrayList<>();
        this.formaPagamento = new FormaPagamento();
        this.entrega = new Entrega();
        this.usuarioInclusao = new Usuario();
        this.usuarioFechamento = new Usuario();
    }

    public Pedido(Cliente cliente, Date dataEntrada, Date dataPrevista, Date dataFechamento, String observação, int quantidadeParcelas, boolean retira, FormaPagamento formaPagamento, List<ItemDePedido> itens, Entrega entrega, Date dataUltimaAtualizacao, Usuario usuarioInclusao, Usuario usuarioFechamento, long id, boolean ativo) {
        super(id, ativo);
        this.cliente = cliente;
        this.dataEntrada = dataEntrada;
        this.dataPrevista = dataPrevista;
        this.dataFechamento = dataFechamento;
        this.observação = observação;
        this.quantidadeParcelas = quantidadeParcelas;
        this.retira = retira;
        this.formaPagamento = formaPagamento;
        this.itens = itens;
        this.entrega = entrega;
        this.dataUltimaAtualizacao = dataUltimaAtualizacao;
        this.usuarioInclusao = usuarioInclusao;
        this.usuarioFechamento = usuarioFechamento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public Date getDataPrevista() {
        return dataPrevista;
    }

    public void setDataPrevista(Date dataPrevista) {
        this.dataPrevista = dataPrevista;
    }

    public Date getDataFechamento() {
        return dataFechamento;
    }

    public void setDataFechamento(Date dataFechamento) {
        this.dataFechamento = dataFechamento;
    }

    public String getObservação() {
        return observação;
    }

    public void setObservação(String observação) {
        this.observação = observação;
    }

    public int getQuantidadeParcelas() {
        return quantidadeParcelas;
    }

    public void setQuantidadeParcelas(int quantidadeParcelas) {
        this.quantidadeParcelas = quantidadeParcelas;
    }

    public boolean isRetira() {
        return retira;
    }

    public void setRetira(boolean retira) {
        this.retira = retira;
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public List<ItemDePedido> getItens() {
        return itens;
    }

    public void setItens(List<ItemDePedido> itens) {
        this.itens = itens;
    }

    public Entrega getEntrega() {
        return entrega;
    }

    public void setEntrega(Entrega entrega) {
        this.entrega = entrega;
    }

    public Date getDataUltimaAtualizacao() {
        return dataUltimaAtualizacao;
    }

    public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
        this.dataUltimaAtualizacao = dataUltimaAtualizacao;
    }

    public Usuario getUsuarioInclusao() {
        return usuarioInclusao;
    }

    public void setUsuarioInclusao(Usuario usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    public Usuario getUsuarioFechamento() {
        return usuarioFechamento;
    }

    public void setUsuarioFechamento(Usuario usuarioFechamento) {
        this.usuarioFechamento = usuarioFechamento;
    }

    public void adicionarItem(ItemDePedido item) {
        this.itens.add(item);
    }

    public void removerItem(ItemDePedido item) {
        this.itens.remove(item);
    }

    public double getTotalPedido() {
        double total = 0;

        for (ItemDePedido iten : itens) {
            total += iten.getTotal();
        }

        return total;
    }

}
