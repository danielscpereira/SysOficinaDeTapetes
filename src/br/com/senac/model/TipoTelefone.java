/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

/**
 *
 * @author sala306b
 */
public class TipoTelefone extends Entidade {

    private String descricao;

    public TipoTelefone() {
    }

    public TipoTelefone(String descricao, long id, boolean ativo) {
        super(id, ativo);
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return this.id + " " +  this.descricao + " " + this.ativo;
    }

}
