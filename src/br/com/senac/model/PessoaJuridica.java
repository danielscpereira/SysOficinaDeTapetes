/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

import java.util.List;

/**
 *
 * @author sala306b
 */
public class PessoaJuridica extends Cliente {

    private String cnpj;
    private String nomeFantasia;
    private String inscricaoEstadual;

    public PessoaJuridica() {
        super();
    }

    public PessoaJuridica(String cnpj, String nomeFantasia, String nome, String inscricaoEstadual, String email, List<Endereco> enderecos, List<Telefone> telefones, long id, boolean ativo) {
        super(nome, email, enderecos, id, ativo);
        this.cnpj = cnpj;
        this.nomeFantasia = nomeFantasia;
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

}
