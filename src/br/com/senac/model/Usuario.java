/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

import java.util.*;

/**
 *
 * @author sala306b
 */
public class Usuario extends PessoaFisica {

    private String login;
    private String senha;
    private boolean bloqueado;
    private boolean trocaSenha;
    private Date dataUltimaAtualizacao;
    private Usuario usuarioUltimaAtualizacao;

    public Usuario(String login, long id, String nome, String cpf){
        this.login = login;
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
    }
 
    public Usuario() {

    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(boolean bloqueado) {
        this.bloqueado = bloqueado;
    }

    public boolean isTrocaSenha() {
        return trocaSenha;
    }

    public void setTrocaSenha(boolean trocaSenha) {
        this.trocaSenha = trocaSenha;
    }

    public Date getDataUltimaAtualizacao() {
        return dataUltimaAtualizacao;
    }

    public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
        this.dataUltimaAtualizacao = dataUltimaAtualizacao;
    }

    public Usuario getUsuarioUltimaAtualizacao() {
        return usuarioUltimaAtualizacao;
    }

    public void setUsuarioUltimaAtualizacao(Usuario usuarioUltimaAtualizacao) {
        this.usuarioUltimaAtualizacao = usuarioUltimaAtualizacao;
    }

}
