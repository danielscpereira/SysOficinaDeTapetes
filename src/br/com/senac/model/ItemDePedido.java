/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

/**
 *
 * @author sala306b
 */
public class ItemDePedido extends Entidade {

    private Servico servico;
    private int quantidade;
    private double precoUnitario;

    public ItemDePedido() {
        this.servico = new Servico();
    }

    public ItemDePedido(Servico servico, int quantidade, double precoUnitario, long id, boolean ativo) {
        super(id, ativo);
        this.servico = servico;
        this.quantidade = quantidade;
        this.precoUnitario = precoUnitario;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(double precoUnitario) {
        this.precoUnitario = precoUnitario;
    }
    
    public double getTotal(){
        return this.quantidade * this.precoUnitario ; 
    }

}
