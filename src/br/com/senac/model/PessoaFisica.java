/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

import java.util.List;

/**
 *
 * @author sala306b
 */
public class PessoaFisica extends Cliente {

    protected String cpf;
    private String rg;

    public PessoaFisica() {
        super();
    }

    public PessoaFisica(long id, boolean ativo) {
        super(id, ativo);
    }

    public PessoaFisica(String cpf, String rg, String nome,String email ,List<Endereco> enderecos, List<Telefone> telefones, long id, boolean ativo) {
        super(nome, email, enderecos, id, ativo);
        this.cpf = cpf;
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

}
