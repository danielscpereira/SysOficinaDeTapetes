/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

/**
 *
 * @author sala306b
 */
public class Uf extends Entidade {

    private String estado;
    private String sigla;

    public Uf() {
    }

    public Uf(long id, boolean ativo, String estado, String sigla) {
        this.id = id;
        this.ativo = ativo;
        this.estado = estado;
        this.sigla = sigla;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

}
