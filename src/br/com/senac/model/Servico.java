/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

/**
 *
 * @author sala306b
 */
public class Servico extends Entidade {

    private String descricao;
    private double precoPadrao;
    private Categoria categoria;

    public Servico() {
        this.categoria = new Categoria();
    }

    public Servico(String descricao, double precoPadrao, Categoria categoria, long id, boolean ativo) {
        super(id, ativo);
        this.descricao = descricao;
        this.precoPadrao = precoPadrao;
        this.categoria = categoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPrecoPadrao() {
        return precoPadrao;
    }

    public void setPrecoPadrao(double precoPadrao) {
        this.precoPadrao = precoPadrao;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

}
