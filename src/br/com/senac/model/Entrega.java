/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

import java.util.Date;

/**
 *
 * @author daniel
 */
public class Entrega extends Entidade {

    private Date dataEntrega;
    private String observacaoes;
    private Endereco endereco;
    private Pedido pedido;

    public Entrega() {
        this.dataEntrega = new Date();
        this.endereco = new Endereco();
    }

    public Entrega(Date dataEntrega, String observacaoes, Endereco endereco, Pedido pedido, long id, boolean ativo) {
        super(id, ativo);
        this.dataEntrega = dataEntrega;
        this.observacaoes = observacaoes;
        this.endereco = endereco;
        this.pedido = pedido;
    }

    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public String getObservacaoes() {
        return observacaoes;
    }

    public void setObservacaoes(String observacaoes) {
        this.observacaoes = observacaoes;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

}
