/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

/**
 *
 * @author daniel
 */
public class Telefone extends Entidade {
    public static String get;
    private String numero;
    private TipoTelefone tipoTelefone;
    private boolean padrao;

    public Telefone() {
        this.tipoTelefone = new TipoTelefone();
    }

    public Telefone(String numero, TipoTelefone tipoTelefone, boolean padrao, long id, boolean ativo) {
        super(id, ativo);
        this.numero = numero;
        this.tipoTelefone = tipoTelefone;
        this.padrao = padrao;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public TipoTelefone getTipoTelefone() {
        return tipoTelefone;
    }

    public void setTipoTelefone(TipoTelefone tipoTelefone) {
        this.tipoTelefone = tipoTelefone;
    }

    public boolean isPadrao() {
        return padrao;
    }

    public void setPadrao(boolean padrao) {
        this.padrao = padrao;
    }

}
